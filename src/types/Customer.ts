export default interface Customer {
  id?: number;
  name: string;
  tel: string;
  point: number;
  startdate: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
