import type Customer from "./Customer";
import type OrderItem from "./OrderItem";

export default interface Order {
  userId: number;
  orderItems: OrderItem[];
}
