import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem {
  productId: number;
  product?: Product;
  name?: string;
  price?: number;
  amount: number;
  total?: number;
  order?: Order;
  createDate?: Date;
  updateDate?: Date;
  deletedDate?: Date;
}
