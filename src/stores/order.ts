import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type OrderItem from "@/types/OrderItem";
import type Product from "@/types/Product";
import type Order from "@/types/Order";
import OrderService from "@/services/order";
export const useOrderStore = defineStore("Order", () => {
  const OrderItem = ref<OrderItem[]>([]);
  const OrderView = ref<OrderItem[]>([]);
  const dialog = ref(false);
  const member = ref();
  const model = ref(false);

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      model.value = false;
    }
  });

  function addNewOrderItem(item: Product) {
    const index = OrderItem.value.findIndex(
      (orderItems) => orderItems.productId === item.id
    );
    console.log(index);
    if (index === -1) {
      OrderView.value.push({
        productId: item.id!,
        product: item,
        price: item.price,
        amount: 1,
        total: item.price,
      });
      //
      OrderItem.value.push({ productId: item.id!, amount: 1 });
    } else {
      console.log(OrderItem);
      OrderItem.value[index].amount++;
      //
      OrderView.value[index].amount++;
      OrderView.value[index].total =
        OrderView.value[index].amount * OrderView.value[index].price!;
    }
  }
  function RemoveOrderItem(item: OrderItem) {
    const index = OrderItem.value.findIndex(
      (orderItems) => orderItems.productId === item.productId
    );
    if (OrderItem.value[index].amount > 1) {
      OrderItem.value[index].amount--;

      //
      OrderView.value[index].amount--;
      OrderView.value[index].total =
        OrderView.value[index].amount * OrderView.value[index].price!;
    } else {
      btnRemoveOrderItem(item);
    }
  }

  function btnRemoveOrderItem(item: OrderItem) {
    const index = OrderItem.value.findIndex(
      (orderItems) => orderItems.productId === item.productId
    );
    OrderItem.value.splice(index, 1);
    OrderView.value.splice(index, 1);
  }
  async function submitOrder(Id?: number) {
    const Order = ref<Order>({ userId: Id!, orderItems: OrderItem.value });
    const res = await OrderService.saveOrder(Order.value);

    OrderView.value = [];
    OrderItem.value = [];
    dialog.value = false;
  }

  return {
    OrderItem,
    addNewOrderItem,
    btnRemoveOrderItem,
    RemoveOrderItem,
    OrderView,
    submitOrder,
    dialog,
    member,
    model,
  };
});
