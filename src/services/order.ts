import type Order from "@/types/Order";
import http from "./axios";

function getOrder() {
  return http.get("/orders");
}
function saveOrder(Order: Order) {
  return http.post("/orders", Order);
}
function updateOrder(id: number, Order: Order) {
  return http.patch(`/orders/${id}`, Order);
}
function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}
export default { getOrder, saveOrder, updateOrder, deleteOrder };
